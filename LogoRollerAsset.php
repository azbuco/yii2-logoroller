<?php

namespace azbuco\logoroller;

use yii\web\AssetBundle;

class LogoRollerAsset extends AssetBundle
{
    public $sourcePath = '@azbuco/logoroller/assets';
    public $css = [];
    public $js = [
        'js/logoroller.js',
    ];
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

}
