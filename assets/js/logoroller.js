/**
 * Simple script for corporate partner logo rolling.
 * usage:
 * var roller = LogoRoller('ID_OF_WRAPPER_ELEMENT', 'URL_TO_IMAGE', {OPTIONS});
 * 
 * The image is a simple image with all the logos below eachother, distributed
 * equally in height.
 * 
 * @param string|jQuery el
 * @param string logoUrl
 * @param object options
 * @returns {LogoRoller.api}
 */

var LogoRoller = function (el, logoUrl, options) {
    var api = {};

    api.defaults = {
        logoHeight: 100,
        delay: 4000,
        animationDuration: 500
    };

    var s = api.settings = $.extend({}, api.defaults, options);

    api.el = el instanceof jQuery ? el : $(el);
    api.image = new Image();
    api.logoCount = 0;
    api.width = 0;
    api.height = 0;

    api.redraw = function () {
        var logow = api.image.width;
        var currentw = api.el.width();
        var ratio = currentw / logow;

        var logoh = api.image.height;
        var currenth = Math.floor(logoh * ratio);
        api.logoCount = logoh / s.logoHeight;

        api.width = currentw;
        api.height = currenth / api.logoCount;

        api.el.css({
            'background-image': 'url(' + api.image.src + ')',
            'background-size': currentw + 'px ' + currenth + 'px',
            'height': api.height + 'px'
        });
    };

    api.enableShuffle = true;

    api.shuffle = function () {
        var n = Math.floor(Math.random() * api.logoCount);
        var pos = n * api.height;
        api.el.css({
            'background-position-y': '-' + pos + 'px'
        });
    };

    var init = function () {
        api.el.css({
            'background-repeat': 'no-repeat',
            'background-position-y': 1000,
            '-webkit-transition': 'background-position-y ' + s.animationDuration + 'ms ease',
            'transition': 'background-position-y ' + s.animationDuration + 'ms ease'
        });

        api.image.onload = function () {
            api.redraw();

            api.shuffle();

            setInterval(function () {
                api.shuffle();
            }, s.delay);
        };

        api.image.src = logoUrl;

        $(window).on('resize', function () {
            api.redraw();
            api.shuffle();
        });
    };

    init();

    return api;
};