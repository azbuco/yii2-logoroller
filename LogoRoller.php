<?php

namespace azbuco\logoroller;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\web\View;

class LogoRoller extends Widget
{
    public $options = [];
    
    public $image = false;
    
    public $clientOptions = [];
    
    public $type = 'div';

    public function init()
    {
        parent::init();

        $this->view->registerAssetBundle(LogoRollerAsset::className());
        
        $this->options = array_merge(['id' => $this->id], $this->options);
        
        
        
        $options = Json::encode($this->clientOptions);
        $id = $this->options['id'];
        $var = 'logoroller_' . \yii\helpers\Inflector::camelize($this->id);

        $js = <<<JS
var $var = LogoRoller('#$id', '$this->image', $options);             
JS;
        $this->view->registerJs($js, View::POS_READY);
    }

    public function run()
    {
        echo Html::beginTag($this->type, $this->options);
        echo Html::endTag('div');
    }
}
